![UNAM](../img/logoFC_2019.png)

## Universidad Nacional Autónoma de México

  

### Facultad de Ciencias

  

#### Redes de computadoras

  

# Práctica 5

  

Profesor: Paulo Contreras Flores
Ayudante: Ulises Manuel Cárdenas
Ayudante Lab: Ismael Andrade Canales

  

# Prerequisitos

:exclamation: Descargar e instala Packet Tracer de acuerdo con tu sistema operativo. [Archivos](/software/)

# Desarrollo

:point_right: Descarga la páctica [aquí](Practica-5.pdf)

![UNAM](../img/logoFC_2019.png)

## Universidad Nacional Autónoma de México

  

### Facultad de Ciencias

  

#### Redes de computadoras

  

# Examen: DNSSEC

  

Profesor: Paulo Contreras Flores

Ayudante: Ulises Manuel Cárdenas

Ayudante Lab: Ismael Andrade Canales

  

# Prerequisitos

:exclamation: Descargar el archivo zip adjunto ya que es parte del examen. [Archivo](preguntaexamenderedes.zip)

  



## Pregunta DNSSEC


Un dispositivo realizó tres consultas DNS del registro A para el dominio  
[www.bancomer.com](http://www.bancomer.com), recibiendo tres respuestas diferentes, los archivos  
answer_1.txt, answer_2.txt y answer_3.txt, contienen cada uno una  
respuesta. También recibió el hash de tipo sha256 para cada una de las  
respuestas, los cuales se encuentran en los archivos hash_answer_1.txt,  
hash_answer_2.txt y hash_answer_3.txt, respectivamente; y las firmas  
digitales para cada una de las respuestas en los archivos  
firma_digital_answer_1.txt.b64, firma_digital_answer_2.txt.b64 y  
firma_digital_answer_3.txt.b64, que están codificados en base64. Además,  
cuenta con la llave pública del servidor DNS, la cual se encuentra en el  
archivo pubDNSkey.pem.  
  
Cada respuesta tiene una dirección IP diferente para el mismo dominio.  
Siga el procedimiento descrito a continuación para contestar las  
preguntas, y para encontrar la dirección IP legítima asociada al dominio  
solicitado.  
  
  
La firma digital se obtiene al cifrar con la llave privada del servidor  
DNS el hash de la respuesta. Entonces, puede verificar la autenticidad  
de la respuesta a través de su firma digital, para esto:  
  
1. Decodifique cada firma digital  
openssl base64 -d -in firma_digital_answer_X.txt.b64 -out firma_digital_answer_X.txt  
  
2. Descifre el hash de la firma digital, utilizando la llave pública del servidor DNS.  
openssl rsautl -verify -in firma_digital_answer_X.txt -pubin -inkey pubDNSkey.pem  
  
Conteste la siguiente pregunta, ¿cuáles respuestas son autenticas?, es  
decir,  
¿cuáles respuestas sí fueron firmadas con la llave privada del DNS?  
a) answer_1.txt  
b) answer_2.txt  
c) answer_3.txt  
  
  
A pesar de tener la firma digital correcta, es necesario conocer si la  
respuesta no fué modificada durante su envío, para esto:  
  
1. Obtenga el hash de cada respuesta  
sha256sum answer_X.txt  
  
2. Muestre el contenido de cada hash recibido y compárelo con el hash de  
cada respuesta.  
more hash_answer_X.txt  
  
  
Conteste la siguiente pregunta, ¿Cuáles de las respuestas que sí fueron  
firmadas con la llave privada del DNS, se mantuvieron íntegras durante  
su transmisión?  
a) answer_1.txt  
b) answer_2.txt  
c) answer_3.txt  
  
Ahora ya puede contestar la pregunta principal, ¿cuál es la dirección IP  
legítima para el dominio [www.bancomer.com](http://www.bancomer.com)?  
a) 132.248.181.248  
b) 23.15.124.234  
c) 8.4.4.2

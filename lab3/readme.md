![# Welcome to StackEdit!](http://pagina.fciencias.unam.mx/sites/default/files/logoFC_2019.png)  
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información



## Práctica 3: código en repositorio en la nube y diferencia entre tráfico HTTP y HTTPS

## Objetivo:


El objetivo es aprender a centralizar el código fuente de un proyecto web en la nube con Git (GitLab), adicionalmente visualizar la diferencia entre tráfico HTTP y HTTPS

[¡Video de Introducción!](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/lab3-introduccion.mp4?expanded=true&viewer=rich)


## Prerequisitos
#### 1. Contar con usuario y contraseña de la cuenta AWS de Ciencias, en caso de no contar con ello no se podrá realizar la práctica, se revisaran los casos de alumnos especiales directamente conmigo 
#### 2. Instancia EC2 corriendo y con su ip elástica 
#### 3. Cuenta de GitLab creada

## Desarrollo
1. Se crea un proyecto con GitLab [creación de repositorio](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/lab3-creacion-repo.mp4?expanded=true&viewer=rich)
2. Se clona el repositorio en la instancia EC2 en AWS, de esta manera el servidor obtiene el código fuente [EC2 clone de repositorio](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/lab3-aws-ec2-git-clone.mp4?expanded=true&viewer=rich)
3. Configuración de apache para leer repositorio, es necesario decirle a apache dónde está el directorio del repo [configuración de Apache en EC2 y git](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/lab3-ec2-apache-conf.mp4?expanded=true&viewer=rich)
4. Configuración de ambiente local (clone) y código del formulario [Ambiente local con git y código fuente ](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/Lab3-ambiente-local-y-codigo.mp4?expanded=true&viewer=rich)
5. Se guarda el formulario en ambiente local,se hace commit y se coloca en el repositorio en la nube (GitLab) [commit y push a gitlab](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/Lab3-formulario.mov?expanded=true&viewer=rich)
6. Análisis de tráfico http con wireshark [formulario y wireshark ](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/Lab3-http-wireshark.mov?expanded=true&viewer=rich)
7. Ver la demostración de https con wireshark [Demostración de trafico por https tls ](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab3/videos/Lab3-https-wireshark.mov?expanded=true&viewer=rich)
## Evaluación

1. Menciona con tus propias palabras las ventajas que tiene centralizar el código fuente con git sin trabajar directamente en el servidor. (como pistas puedes mencionar el trabajo en equipo entre varios desarrolladores, el escalamiento, automatización etc.)
2. Menciona algún concepto que no te haya quedado del todo claro (opcional)
3. Liga del repositorio GitLab de tu formulario.
4. Captura del trafico http (no seguro) con wireshark.
5. Comentarios de lo visto en esta practica (opcional y libre)

### Notas adicionales

1. El reporte se entrega de manera individual.
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica a Moodle

### Errores comunes

El error 401 "Forbiden" o  'Unauthorized' indica que el recurso al que se quiere acceder por por GET,  POST ... no tiene permisos suficientes 

esto ocurre por dos cosas:

1. el directorio donde esta nuestro repositorio "labredes-web" está creado con permisos que no son de escritura, lectura 
2. el directorio no pertenece a los usuarios/grupos autorizados ( el grupo de apache es www-data )

Entonces para solucionarlo es necesario :

verificar que en el archivo de configuración: 

```
 /etc/apache2/sites-enabled/xxx.conf
```

 exista la ruta al directorio de nuestro repositorio 

```
/home/ubuntu/xxxx
```

si usaron el archivo de recursos de la práctica notarán que la carpeta la llamé **labredes-web**



si está con el nombre y ruta correctos habra que verificar los permisos y propietario con el comando: 

```
sudo chmod -R 775 /home/ubuntu/xxxx
```

de esta manera podemos cambiar de manera recursiva (-R) que el usuario autor y el grupo tendrán permisos de escritura, lectura y ejecución.

​		posteriormente habrá que habilitar que www-data sea parte del grupo con permisos, de la siguiente manera

```
sudo chown -R ubuntu:www-data /home/ubuntu/xxxx
```



Nota. todo lo anterior es en la instancia Ec2 de amazon

Saludos
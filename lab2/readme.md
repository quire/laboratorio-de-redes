![# Welcome to StackEdit!](http://pagina.fciencias.unam.mx/sites/default/files/logoFC_2019.png)  
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información



## Práctica 2: Servidor web con AWS EC2 

## Objetivo:

El alumno aprenderá a crear un servidor web (Apache 2) en la nube de AWS del tipo IaaS (EC2) , además preparará la infraestructura necesaria para un flujo de trabajo en la nube tipo C.D ( Entrega continua ) elemento fundamental de la metodología DevOps.

Finalmente el alumno comprenderá de manera práctica los conceptos de nube, IaaS (Infraestructura como servicio), IP Pública, puertos, politicas de seguridad

## Introducción

El protocolo HTTPS (HyperText Transfer Protocol Secure) es un protocolo de la Capa de aplicación que permite el envió de información cifrada usando los protocolos HTTP y SSL/TLS. El protocolo HTTP envía información en claro a través del medio, el protocolo SSL/TLS es el encargado de encapsular el protocolo HTTP para ser enviado de manera cifrada.

En está práctica utilizaremos estos conceptos colocando un servicio visible desde internet, usando la nube de amazon, en particular el servicio EC2, instalaremos un servidor web Apache 2 y colocaremos una página web de prueba (hola mundo)


## Prerequisitos
#### 1. Contar con usuario y contraseña de la cuenta AWS de Ciencias, en caso de no contar con ello no se podrá realizar la práctica, se revisaran los casos de alumnos especiales directamente conmigo 
#### 2. [*Informativo únicamente ya que la cuenta de alumno no tiene habilitada esta opción] [Crear usuario Administrador, sólo informatívo](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-crear-usuario.mp4?expanded=true&viewer=rich)

## Desarrollo
1. Ingresar a la consola de administración de AWS de la siguiente manera:
    - Ingresar a la siguiente [liga](https://www.awseducate.com/student/s/awssite)
    - Aceptar los términos y condiciones
    - Ingresar a la consola de administración (Consola aws)
    - Es importante que en select de región (parte superior derecha) se elija **North Virginia**
    
2. Es necesario crear una máquina AWS EC2 de la siguiente manera  [Crear Instancia EC2](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-ec2.mp4?expanded=true&viewer=rich)

3. Será necesario configurar el servidor apache (servidor web) de la siguiente manera: [Instalación de Servidor web Apache](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-apache2.mp4?expanded=true&viewer=rich)

4. Finalmente crear nuestro sitio web de prueba (hola mundo) en la siguiete liga: [Hola Mundo](https://gitlab.com/ismael.andrade/laboratorio-de-redes/blob/master/lab2/videos/lab2-hola-mundo.mp4?expanded=true&viewer=rich)

5. Crear un formulario html o lenguaje de programación elegido con los campos:

   - usuario
   - contraseña
   - botón enviar  (que envíe una petición post a cualquier sitio)
     **Nota:  este html debe sustituir el index.html creado en el paso 5**



## Ip Elástica (aws)

1. Para tener una ip fija que no cambie, otorgada por AWS, es necesario haber concluido los pasos anteriores.
2. Ingresar a la consola de AWS Servicios > Ec2 > Networking & Security > Elastic IPs
3. Crear una nueva IP Elástica
4. Una vez creada, seleccionar la ip elástica creada y en el botón de 'actions/acciones' seleccionar 'Asociar dirección/Associate Address'
5. Elegir la instancia EC2 AWS creada y guardar cambios 
6. la página web debe ser visible con la ip elástica creada

## Evaluación

1. ¿Qué es el concepto de nube y a qué se refiere el término Iaas? (describir con sus propias palabras aunque sea un renglón)
2. ¿Qué ventajas observas al utilizar la infraestructura que utilizamos en esta práctica? (describir con sus propias palabras aunque sea un renglón)
3. Colocar la url de amazon donde pueda visualizar el servicio creado en esta práctica ( ver sección anterior ip elástica)
4. Colocar comentarios sobre la práctica (facilidad de ejecución, valoración de lo aprendido)

### Notas adicionales

1. El reporte se entrega de manera individual.
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica a Moodle

### Errores comunes
1. Algunos alumnos con la cuenta ya habilitada de awsEducate, no lograron ingresar a la consola de servicios, para ello podrían seguir los siguientes pasos.
- ingresar a la siguiente [liga](https://www.awseducate.com/student/s/awssite)
- aceptar los términos y condiciones
- Ingresar a la consola de administración es importante que en select de regioń en la parte superior se elija **North Virginia** 
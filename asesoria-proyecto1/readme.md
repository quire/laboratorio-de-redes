![UNAM](../img/logoFC_2019.png)
## Universidad Nacional Autónoma de México

### Facultad de Ciencias

#### Redes de computadoras

# Proyecto 1:  AWS VPC (Arquitectura)

Profesor: Paulo Contreras Flores
Ayudante: Ulises Manuel Cárdenas
Ayudante Lab: Ismael Andrade Canales

# Asesoria Proyecto 1
 :point_right: Coloco la lista de videos de asesoria para el proyecto 1, favor de ver en orden (numero inicial)

 :tv: [videos de asesoria para el proyecto 1](https://www.youtube.com/playlist?list=PLqN25lewrpoYmP3xSpMwHJb3vRnDYYyQ-)
 
### :exclamation: complemento video 3

En el video 3 falta la explicacion de la tabla de ruteo para la subrede pública (no tendrán internet sin esto)

1. Ir al menú de subredes
2. Seleccionar la subred publica
3. en la seccion de abajo, en el tab de routetables, seleccionar el link del identificador de la routetable
4. Esto nos redirige al menu de route table, elegimos acciones > editar rutas
5. colocamos la ruta 0.0.0.0/0  y en target colocamos el Gateway de internet creado
6. hacemos lo mismo para ::/0 (osea ip v6)
7. guardamos y ya debemos ver la nueva ruta en la tabla de ruteo de la subred pública
8. si ya crearon su NACL ya deberían tener salida a internet desde la subred pública
 
 :chicken: [Guia para configurar Route 53 como DNS Privado](guia_route53_dns_privado.pdf) (al parecer, si es posible con la cuenta aws de ciencias)
 
 ## Solucion a problema con NAT gateway (salida a internet desde la red privada)
 Coloco video para solucionar la salida a internet de la red privada usando squid
 
 una vez configurado squid como se muestra en el video se debe ejecutar
 
 `sudo service squid start`
 
 :tv: [video Solucion a problema con NAT Gateway](https://youtu.be/CbhjyGl-WcE)
## Registro DKIM
:zap:
Esta guía en español me pareció fácil de seguir [guía](https://www.magiconline.es/ayuda/como-configurar-dkim-en-una-maquina-con-postfix/)

:unamused: no solo sigan la guía recuerden que deben cambiar sus valores correspondientes. por ejemplo 201601 (es año y mes actual)  dominiodejemplo.com (es su dominio)

## Registro SPF
Es trivial se deja como ejercicio al lector


# Diagrama 

![Diagrama](diagrama-proyecto1.svg)

